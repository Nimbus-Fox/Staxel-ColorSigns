﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.ColorSigns.Components {
    public class ColorSignComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "colorSign";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new ColorSignComponent(config);
        }
    }
}