﻿using Microsoft.Xna.Framework;
using NimbusFox.KitsuneCore;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.ColorSigns.Components {
    public class ColorSignComponent {
        public Vector3F TextOffset { get; }
        public Vector2I Size { get; }
        public bool Debug { get; }
        public Color DebugColor { get; } = Color.White;

        public ColorSignComponent(Blob config) {
            TextOffset = config.Contains("textOffset") ? config.FetchBlob("textOffset").GetVector3F() : Vector3F.Zero;
            
            TextOffset += new Vector3F(0.5f, 0.95f, 0.5f);

            Size = config.Contains("size") ? config.FetchBlob("size").GetVector2I() : new Vector2I(100, 100);

            Debug = config.GetBool("debug", false);

            if (Debug) {
                if (config.Contains("debugColor")) {
                    DebugColor = config.GetColor("debugColor");
                }
            }
        }
    }
}