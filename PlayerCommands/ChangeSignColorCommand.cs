﻿using NimbusFox.ColorSigns.Tiles.ColorSignTile;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.ColorSigns.PlayerCommands {
    public class ChangeSignColorCommand : IPlayerExtendedCommand {
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorSigns.command.changeSignColor";
        }

        public void Invoke(PlayerEntityLogic logic, Entity entity, Blob extendedActionBlob, Timestep timestep,
            EntityUniverseFacade facade) {
            if (extendedActionBlob.Contains("tileLocation")) {
                if (facade.TryFetchTileStateEntityLogic(extendedActionBlob.FetchBlob("tileLocation").GetVector3I(),
                    TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, entity.Id, out var tileLogic)) {
                    if (tileLogic is ColorSignTileStateLogic signLogic) {
                        if (extendedActionBlob.Contains("blockColor")) {
                            signLogic.ChangeSignColor(extendedActionBlob.GetColor("blockColor"));
                        }
                    }
                }
            }
        }
    }
}