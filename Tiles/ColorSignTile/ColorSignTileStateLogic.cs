﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.ColorSigns.Components;
using NimbusFox.ColorSigns.Effects;
using NimbusFox.KitsuneCore;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Components;
using NimbusFox.KitsuneCore.V2.Classes;
using NimbusFox.KitsuneCore.V2.UI;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;
using Constants = NimbusFox.KitsuneCore.V2.Constants;
using Extensions = NimbusFox.KitsuneCore.Extensions;
using Helpers = NimbusFox.KitsuneCore.Helpers;

namespace NimbusFox.ColorSigns.Tiles.ColorSignTile {
    public class ColorSignTileStateLogic : TileStateEntityLogic {
        public MultipleMatrixDrawable Drawable { get; private set; }
        private Color TargetColor = Color.Yellow;
        public TileConfiguration TileConfig;
        private VoxelRecolorComponent _recolorComponent;
        public ColorSignComponent SignComponent;
        private readonly Dictionary<Color, Color> _recolors = new Dictionary<Color, Color>();
        private readonly Blob _constructionBlob = BlobAllocator.Blob(true);
        private bool _updateClient = true;
        public float Rotation = 0;
        public Vector3F Offset = new Vector3F();
        public TextureRectangleDrawable TextDrawable { get; private set; }
        private string _text = "Hello World";

        public ColorSignTileStateLogic(Entity entity) : base(entity) { }

        public void NeedsUpdate() {
            _updateClient = true;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration.Code != TileConfig.Code) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructionBlob.AssignFrom(arguments);
            Entity.Physics.MakePhysicsless();
            Entity.Physics.ForcedPosition(arguments.FetchBlob("location").GetVector3I().ToTileCenterVector3D());
            Location = arguments.FetchBlob("location").GetVector3I();
            TileConfig = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
            _recolorComponent = TileConfig?.Components.GetOrDefault<VoxelRecolorComponent>();
            SignComponent = TileConfig?.Components.GetOrDefault<ColorSignComponent>();

            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                Entity.Id, out var tile)) {
                Rotation = tile.Configuration.GetRotationInRadians(tile.Variant());
            }

            NeedsUpdate();
        }

        public override void Bind() { }

        public override bool Interactable() {
            return true;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main,
            ControlState alt) {
            if (alt.DownClick) {
                var data = BlobAllocator.Blob(true);
                data.SetLong("target", entity.Id.Id);
                Extensions.SetColor(data, "blockColor", TargetColor);
                data.FetchBlob("tileEntity").SetVector3I(Location);
                entity.Effects.Start(new EffectTrigger(ShowColorPickerEffectBuilder.KindCode(), data));
            }
        }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }

        public override void BeingLookedAt(Entity entity) { }

        public override bool IsBeingLookedAt() {
            return false;
        }

        private void RecolorSign(Color color) {
            if (!Helpers.IsClient()) {
                return;
            }

            foreach (var col in _recolorComponent.ColorCoords.Keys) {
                var shade = new Color(byte.MaxValue - col.R, byte.MaxValue - col.G, byte.MaxValue - col.B);
                _recolors[col] = new Color(
                    Helpers.GetNewByte(color.R, shade.R),
                    Helpers.GetNewByte(color.G, shade.G),
                    Helpers.GetNewByte(color.B, shade.B)
                );
            }

            Drawable = VoxelRecolor.FetchDrawable(TileConfig, _recolors);
        }

        public override void Store() {
            base.Store();
            if (_updateClient) {
                Store(Entity.Blob.FetchBlob("colorSigns"));
                _updateClient = false;
            }
        }

        private void Store(Blob blob) {
            Extensions.SetColor(blob, "newColor", TargetColor);
            blob.SetString("tile", TileConfig.Code);
            blob.FetchBlob("rotation").SetVector3F(new Vector3F(Rotation, 0f, 0f));
            blob.FetchBlob("location").SetVector3I(Location);
            blob.SetString("text", _text);
        }

        public override void StorePersistenceData(Blob data) {
            base.StorePersistenceData(data);
            data.FetchBlob("constructionData").AssignFrom(_constructionBlob);
            Store(data.FetchBlob("colorSigns"));
        }

        public override void Restore() {
            base.Restore();
            Restore(Entity.Blob.FetchBlob("colorSigns"));
        }

        private void Restore(Blob blob) {
            if (blob.Contains("tile") && TileConfig == null) {
                TileConfig = GameContext.TileDatabase.GetTileConfiguration(blob.GetString("tile"));
                _recolorComponent = TileConfig?.Components.GetOrDefault<VoxelRecolorComponent>();
                SignComponent = TileConfig?.Components.GetOrDefault<ColorSignComponent>();

                if (_recolorComponent == null) {
                    return;
                }

                _recolors.Clear();

                foreach (var color in _recolorComponent.ColorCoords.Keys) {
                    _recolors.Add(color, color);
                }

                if (SignComponent == null) {
                    return;
                }

                if (blob.Contains("text")) {
                    if (TextDrawable == null) {
                        TextDrawable = new TextureRectangleDrawable(SignComponent.Size.ToVector2F().ToVector2(),
                            new UIPicture(context => {
                                var texture = new Texture2D(context.Graphics.GraphicsDevice, SignComponent.Size.X * 10,
                                    SignComponent.Size.Y * 10);

                                if (SignComponent.Debug) {
                                    texture.Clear(SignComponent.DebugColor);
                                } else {
                                    if (UIManager.TryFetchFont(Constants.Fonts.MyFirstCrush, out var font)) {
                                        var text = font.RenderString(_text).ToTexture2D(context);

                                        texture = new Texture2D(context.Graphics.GraphicsDevice, text.Width,
                                            text.Height * 10);

                                        texture.Paint(text, Vector2I.Zero);
                                    }
                                }

                                return texture;
                            }));
                    } else {
                        TextDrawable.Redraw();
                    }
                }
            }

            if (blob.Contains("rotation")) {
                Rotation = blob.FetchBlob("rotation").GetVector3F().X;
            }

            if (blob.Contains("location")) {
                Location = blob.FetchBlob("location").GetVector3I();
            }

            if (blob.Contains("newColor") || Drawable == null) {
                TargetColor = Extensions.GetColor(blob, "newColor");
                RecolorSign(TargetColor);

                if (Drawable == null) {
                    RecolorSign(TargetColor);
                }
            }
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            if (data.Contains("constructionData")) {
                Construct(data.FetchBlob("constructionData"), facade);
            }

            var blob = data.FetchBlob("colorSigns");

            Restore(blob);
        }

        public void ChangeSignColor(Color color) {
            TargetColor = color;
            NeedsUpdate();
        }
    }
}