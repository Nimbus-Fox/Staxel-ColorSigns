﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore.V2.Classes;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.ColorSigns.Tiles.ColorSignTile {
    public class ColorSignTileStatePainter : EntityPainter {
        protected override void Dispose(bool disposing) { }

        public ColorSignTileStatePainter() {}

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade,
            int updateSteps) { }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) {
            if (entity.Logic is ColorSignTileStateLogic logic) {
                if (logic.TileConfig.AttachToBelow) {
                    if (facade.ReadTile(logic.Location - new Vector3I(0, 1, 0), TileAccessFlags.None,
                        ChunkFetchKind.LivingWorld, entity.Id,
                        out var tile)) {
                        logic.Offset.Y = tile.Configuration.TopOffset;
                    }
                } else if (logic.TileConfig.AttachToAbove) {
                    if (facade.ReadTile(logic.Location + new Vector3I(0, 1, 0), TileAccessFlags.None,
                        ChunkFetchKind.LivingWorld, entity.Id, out var tiles)) {
                        logic.Offset.Y = tiles.Configuration.BottomOffset;
                    }
                }
            }
        }

        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController,
            Timestep renderTimestep) { }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (entity.Logic is ColorSignTileStateLogic logic) {
                if (logic.Drawable != null) {
                    var targetMatrix = matrix
                        .Translate(logic.Drawable.VoxelOffsets)
                        .RotateUnitY(logic.Rotation)
                        .Translate(-logic.Drawable.VoxelOffsets)
                        .Translate((logic.Location.ToVector3F() + logic.Offset) - renderOrigin.ToVector3F());
                    logic.Drawable.Render(graphics, ref targetMatrix);

                    if (logic.SignComponent != null) {
                        var textOffset = new Vector3F(logic.SignComponent.TextOffset.X,
                            logic.SignComponent.TextOffset.Y, logic.SignComponent.TextOffset.Z);

                        var textMatrix = matrix.Translate(textOffset).Multiply(ref targetMatrix);

                        logic.TextDrawable.Render(graphics, ref textMatrix);
                    }
                }
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}