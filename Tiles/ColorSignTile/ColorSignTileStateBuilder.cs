﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ColorSigns.Tiles.ColorSignTile {
    public class ColorSignTileStateBuilder : ITileStateBuilder {
        public void Dispose() {
            
        }

        public void Load() {
            
        }

        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.colorSigns.tileState.colorSignTile";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return ColorSignTileStateEntityBuilder.Spawn(location, universe, tile);
        }
    }
}