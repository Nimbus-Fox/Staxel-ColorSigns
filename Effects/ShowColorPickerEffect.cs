﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using NimbusFox.ColorSigns.PlayerCommands;
using NimbusFox.KitsuneCore;
using NimbusFox.KitsuneCore.V1.UI.Classes;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.ColorSigns.Effects {
    class ShowColorPickerEffect : IEffect, IDisposable {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public bool Completed() {
            return _completed;
        }

        private Color _color = Color.White;
        private Vector3I _tileLocation;

        public ShowColorPickerEffect(Blob data, EntityUniverseFacade facade) {
            if (!data.Contains("target")) {
                return;
            }

            var players = new Lyst<Entity>();

            facade.GetPlayers(players);

            foreach (var player in players) {
                if (ClientContext.PlayerFacade.IsLocalPlayer(player)) {
                    if (player.Id.Id == data.GetLong("target")) {
                        _show = true;
                        break;
                    }
                }
            }

            if (!_show) {
                return;
            }

            if (data.Contains("blockColor")) {
                _color = data.GetColor("blockColor");
            }

            if (data.Contains("tileEntity")) {
                _tileLocation = data.FetchBlob("tileEntity").GetVector3I();
            }
        }

        private bool _completed;

        private bool _show;

        private static ColorPickerWindow _current;

        public void Render(Entity entity, EntityPainter painter, Timestep renderTimestep, DeviceContext graphics,
            ref Matrix4F matrix,
            Vector3D renderOrigin, Vector3D position, RenderMode renderMode) {
            if (!_show) {
                _completed = true;
                return;
            }
            if (!Completed()) {
                _completed = true;

                _current?.Dispose();

                _current = new ColorPickerWindow(_color);

                _current.OnColorSet += color => {
                    var cmd = BlobAllocator.Blob(true);
                    cmd.SetString("action", ChangeSignColorCommand.KindCode());
                    cmd.FetchBlob("tileLocation").SetVector3I(_tileLocation);
                    cmd.SetLong("blockColor", color.PackedValue);

                    ClientContext.OverlayController.AddCommand(cmd);
                };

                _current.OnClose += () => {
                    ClientContext.WebOverlayRenderer.ReleaseInputControl();
                };

                _current.Show();

                ClientContext.WebOverlayRenderer.AcquireInputControl();
            }
        }

        

        public void Stop() { }
        public void Pause() { }
        public void Resume() { }
    }
}
